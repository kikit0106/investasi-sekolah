<?php

return [
    'adminEmail' => 'admin@investasi.com',
    'user.passwordResetTokenExpire' => 3600,
];
