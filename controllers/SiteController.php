<?php

namespace app\controllers;

use app\models\PasswordForm;
use app\models\ResetPasswordForm;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('site/login');
        }

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays error page.
     *
     * @return string
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        $name = $exception->getCode();
        $message = $exception->getMessage();

        if (Yii::$app->user->isGuest) {
            $this->layout = '@app/views/layouts/main-login';
        }

        return $this->render('error', compact('name', 'message'));
    }

    public function actionChangePassword()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                try {
                    $user = User::findByUsername(Yii::$app->user->identity->username);

                    $user->updatePassword($model->newpass);

                    if ($user->save()) {
                        Yii::$app->session->setFlash('success', 'Password berhasil diubah.');
                    } else {
                        Yii::$app->session->setFlash('error', 'Password gagal diubah.');
                    }

                    return $this->redirect('change-password');

                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', "{$e->getMessage()}");

                    return $this->render('change-password', compact('model'));
                }
            } else {
                return $this->render('change-password', compact('model'));
            }

        } else {
            return $this->render('change-password', compact('model'));
        }
    }

    public function actionForgotPassword()
    {
        $this->layout = '@app/views/layouts/main-login';

        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            if($user = $model->findByEmail()){
                $user->generatePasswordResetToken();
                if ($user->save()){
                    $mail = Yii::$app->mailer->compose('redirect', ['model' => $user])
                        ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name ])
                        ->setTo($user->email)
                        ->setSubject('Password reset untuk ' . Yii::$app->name)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Permintaan reset password berhasil dikirim.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Email tidak terdaftar.');
            }
        }

        return $this->render('forgot-password',compact('model'));
    }

    public function actionResetPassword($token)
    {
        $this->layout = '@app/views/layouts/main-login';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->resetPassword()){
                Yii::$app->session->setFlash('success', 'Password baru telah tersimpan.');

                return $this->goHome();
            }
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }
}
