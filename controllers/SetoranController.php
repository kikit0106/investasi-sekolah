<?php

namespace app\controllers;

use app\models\Peserta;
use app\models\Setoran;
use app\models\SetoranSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * SetoranController implements the CRUD actions for Setoran model.
 */
class SetoranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Setoran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SetoranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Setoran model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Setoran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpload()
    {
        $model = new Setoran();

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file) {
                $path = '/web/uploads/files/';
                $file_import = rand(0, 20) . '-' . str_replace('', '-', $model->file->name);

                $bulkInsertArray = array();

                $model->file->saveAs(Yii::$app->basePath . $path . $file_import);

                $loc = Yii::getAlias('@webroot/uploads/files/');
                $handle = fopen($loc . $file_import, 'r');
                if ($handle) {

                    $peserta = array_flip(Peserta::getArrayMap());
                    $n = $m = 0;

                    while (($line = fgetcsv($handle, 1000, ",")) != false) {
                        $no_induk = $line[0];
                        if (!empty($line[2])) {
                            for ($i = 1; $i < sizeof($line); $i+=2) {
                                if (!empty($line[$i + 1])){
                                    $bulkInsertArray[] = [
                                        'peserta' => $peserta[$no_induk],
                                        'nominal' => intval($line[$i + 1]),
                                        'tipe' => 2,
                                        'keterangan' => 'input by code',
                                        'created_at' => date('Y-m-d'),
                                        'tanggal' => Setoran::BULAN_AJARAN[ intval($i / 2) + 1] . '-' .
                                            (empty($line[$i]) ? '01' : intval($line[$i]))
                                    ];
                                }
                            }
                        }
                    }
                    fclose($handle);

                    $tableName = 'tbl_setoran';
                    $columnNameArray = ['peserta', 'nominal', 'tipe', 'keterangan', 'created_at', 'tanggal'];

                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert($tableName, $columnNameArray, $bulkInsertArray)
                        ->execute();
                }
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('_upload', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new Setoran();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Data Berhasil Ditambahkan.');
            return $this->redirect(['/peserta/index']);
        }

        if (isset($_GET['peserta']) && $_GET['peserta']) {
            $model->peserta = $_GET['peserta'];
        }

        $peserta = Peserta::findOne(['id_peserta' => $model->peserta]);

        return $this->render('create', compact('model', 'peserta'));
    }

    /**
     * Updates an existing Setoran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_setoran]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Setoran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()){
            Yii::$app->session->setFlash('success', 'Data Berhasil Dihapus.');
        }

        return $this->redirect(['/peserta/index']);
    }

    /**
     * Finds the Setoran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Setoran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Setoran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
