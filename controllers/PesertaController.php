<?php

namespace app\controllers;

use app\components\Helper;
use app\models\Kartu;
use app\models\Kelas;
use app\models\Peserta;
use app\models\PesertaSearch;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * PesertaController implements the CRUD actions for Peserta model.
 */
class PesertaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Peserta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PesertaSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $kelas = Kelas::getArrayMap();
        $kartu = Kartu::getArrayMap();

        return $this->render('index', compact('searchModel', 'dataProvider', 'kelas', 'kartu'));
    }

    /**
     * Displays a single Peserta model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        //dump($model->investasiR); exit();

        return $this->renderAjax('view', compact('model'));
    }

    /**
     * Creates a new Peserta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpload()
    {
        $model = new Peserta();

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->file) {
                $path = '/web/uploads/files/';
                $file_import = rand(0, 20) . '-' . str_replace('', '-', $model->file->name);

                $bulkInsertArray = array();

                $model->file->saveAs(Yii::$app->basePath . $path . $file_import);

                $loc = Yii::getAlias('@webroot/uploads/files/');
                $handle = fopen($loc . $file_import, 'r');
                if ($handle) {

                    while (($line = fgetcsv($handle, 2000, ",")) != false) {
                        $bulkInsertArray[] = [
                            'no_induk' => $line[0],
                            'nama' => $line[1],
                            'jk' => $line[2],
                            'kelas' => $line[3],
                            'kartu' => $line[4],
                            'psm' => $line[5],
                            'investasi' => $line[6],
                        ];

                    }
                    fclose($handle);

                    $tableName = 'tbl_peserta';
                    $columnNameArray = ['no_induk', 'nama', 'jk', 'kelas', 'kartu', 'psm', 'investasi'];
                    Yii::$app->db->createCommand()->batchInsert($tableName, $columnNameArray,
                        $bulkInsertArray)->execute();
                }
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('_upload', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
        $model = new Peserta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Data Berhasil Ditambahkan.');
            return $this->redirect(['index']);
        }

        $kelas = Kelas::getArrayMap();
        $kartu = Kartu::getArrayMap();
        return $this->render('create', compact('model', 'kelas', 'kartu'));
    }

    /**
     * Updates an existing Peserta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_peserta]);
        } else {
            $kelas = Kelas::getArrayMap();
            $kartu = Kartu::getArrayMap();
            return $this->render('update', compact('model', 'kartu', 'kelas'));
        }
    }

    /**
     * Deletes an existing Peserta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionExcel()
    {
        $searchModel = new PesertaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $data = $dataProvider->query->all();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getActiveSheet()->setTitle("Daftar Peserta");

        $spreadsheet->getActiveSheet()// set header
        ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'No. Induk')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'L/P')
            ->setCellValue('E1', 'Kelas')
            ->setCellValue('F1', 'Kartu')
            ->setCellValue('G1', 'PSM')
            ->setCellValue('H1', 'Investasi')
            ->setCellValue('I1', 'Total PSM')
            ->setCellValue('J1', 'Total Investasi');

        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->getColor()->setARGB(Color::COLOR_DARKGREEN);
        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
        foreach (range('A', 'J') as $i => $word) {
            $spreadsheet->getActiveSheet()->getColumnDimension($word)->setAutoSize(true);
        }
        $spreadsheet->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode('Rp #,###.00');
        $spreadsheet->getActiveSheet()->getStyle('J')->getNumberFormat()->setFormatCode('Rp #,###.00');
        $spreadsheet->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode('Rp #,###.00');
        $spreadsheet->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode('Rp #,###.00');

        foreach ($data as $key => $val) {
            $invest = array_sum(ArrayHelper::map($val->investasiR, 'id_setoran', 'nominal'));
            $psm = array_sum(ArrayHelper::map($val->psmR, 'id_setoran', 'nominal'));

            $comment1 = $comment2 = '';

            foreach ($val->psmR as $i => $d) {
                $comment1 .= Helper::formatDateIndonesia($d->tanggal) . ' ' . Helper::rupiah_display($d->nominal) . " \r\n";
            }

            foreach ($val->investasiR as $x => $y) {
                $comment2 .= Helper::formatDateIndonesia($y->tanggal) . ' ' . Helper::rupiah_display($y->nominal) . " \r\n";
            }

            $spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . ($key + 2), $key + 1);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('B' . ($key + 2), $val->no_induk);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('C' . ($key + 2), $val->nama);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('D' . ($key + 2), $val->jk);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . ($key + 2), $val->kelasR->nama);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . ($key + 2), $val->kartu == 4
                ? '' : $val->kartuR->nama);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . ($key + 2), $val->psm);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . ($key + 2), $val->investasi);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . ($key + 2), $psm);
            $spreadsheet->setActiveSheetIndex(0)->setCellValue('J' . ($key + 2), $invest);
            $spreadsheet->getActiveSheet()
                ->getComment('I' . ($key + 2))
                ->getText()->createText('Setoran:');
            $spreadsheet->getActiveSheet()
                ->getComment('I' . ($key + 2))
                ->getText()->createText("\r\n");
            $spreadsheet->getActiveSheet()
                ->getComment('I' . ($key + 2))
                ->getText()->createText($comment1);
            $spreadsheet->getActiveSheet()
                ->getComment('J' . ($key + 2))
                ->getText()->createText('Setoran:');
            $spreadsheet->getActiveSheet()
                ->getComment('J' . ($key + 2))
                ->getText()->createText("\r\n");
            $spreadsheet->getActiveSheet()
                ->getComment('J' . ($key + 2))
                ->getText()->createText($comment2);
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Daftar Peserta.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

        return $writer->save('php://output');
    }

    /**
     * Finds the Peserta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Peserta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Peserta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
