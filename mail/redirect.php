<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 4/7/2017
 * Time: 11:00 AM
 */

/* @var $model \app\models\User */

use yii\helpers\Url;

?>
<div>
    <p> Gunakan link ini untuk melakukan reset password : <?= Url::to(["/site/reset-password", 'token' =>
            $model->password_reset_token],
            true); ?> </p>
</div>