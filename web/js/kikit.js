var input = $('.input-rupiah');

input.on('keyup', function (e) {
    var bilangan = $(this).val();
    var rupiah = splitMoney(bilangan);
    return $(this).val(rupiah);
});

input.on('keydown', function (event) {
    {
        var key = event.which || event.keyCode;
        if (key != 188 // Comma
            && key != 8 // Backspace
            && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
            && (key < 48 || key > 57) // Non digit
        ) {
            event.preventDefault();
        }
    }
});

$('#im_bul').on('keyup',function () {
    var im_bul = parseInt(clean($(this).val()));
    var im_tot = parseInt(clean($('#im_tot').val()));

    if (im_bul > im_tot){
        var pjg = im_bul.toString().length;
        var str = im_bul.toString().substring(0,(pjg-1));
        $(this).val(splitMoney(str));

        alert('Peringatan!, Limit per bulan lebih besar dari total reimburse.');
    }
});

function clean(money) {
    var rupiah = money;
    var clean = rupiah.replace(/\D/g, '');
    return clean;
}

function splitMoney(bilangan) {
    var number_string = bilangan.replace(/[^,\d]/g, '').toString();
    var split = number_string.split(',');
    var sisa = split[0].length % 3;
    var rupiah = split[0].substr(0, sisa);
    var ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        var separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

    return 'Rp. ' +rupiah
}