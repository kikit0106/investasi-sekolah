<?php

namespace app\models;

/**
 * This is the model class for table "tbl_setoran".
 *
 * @property string $id_setoran
 * @property string $peserta
 * @property string $nominal
 * @property string $tipe
 * @property string $keterangan
 * @property string $tanggal
 * @property string $created_at
 *
 * @property Peserta $pesertaR
 */
class Setoran extends \yii\db\ActiveRecord
{
    public $file;
    const TIPE = [
        1 => 'investasi',
        2 => 'psm'
    ];
    const BULAN_AJARAN = [
        1 => '2017-07',
        2 => '2017-08',
        3 => '2017-09',
        4 => '2017-10',
        5 => '2017-11',
        6 => '2017-12',
        7 => '2018-01',
        8 => '2018-02',
        9 => '2018-03',
        10 => '2018-04',
        11 => '2018-05',
        12 => '2018-06',
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_setoran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal','nominal','tipe'], 'required'],
            [['created_at','nominal', 'peserta', 'keterangan'], 'safe'],
            [['keterangan'], 'string', 'max' => 255],
            [['peserta'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['peserta' => 'id_peserta']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_setoran' => 'Id Setoran',
            'peserta' => 'Peserta',
            'nominal' => 'Nominal',
            'keterangan' => 'Keterangan',
            'tipe' => 'Jenis Setoran',
            'tanggal' => 'Tanggal',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertaR()
    {
        return $this->hasOne(Peserta::className(), ['id_peserta' => 'peserta']);
    }

    public function beforeSave($insert)
    {
        $this->tanggal = date('Y-m-d', strtotime($this->tanggal));
        $this->nominal = preg_replace('/\D/', '', $this->nominal);

        if ($this->isNewRecord){
            $this->created_at = date('Y-m-d');
        }

        return parent::beforeSave($insert);
    }
}
