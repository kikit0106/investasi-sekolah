<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_peserta".
 *
 * @property string $id_peserta
 * @property string $no_induk
 * @property string $nama
 * @property string $jk
 * @property integer $psm
 * @property integer $investasi
 * @property string $kelas
 * @property string $kartu
 *
 * @property Kartu $kartuR
 * @property Kelas $kelasR
 * @property Setoran[] $setorans
 * @property Setoran[] $investasiR
 * @property Setoran[] $psmR
 */
class Peserta extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_peserta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_induk', 'nama', 'jk', 'kelas', 'kartu'], 'required'],
            [['kelas', 'kartu'], 'integer'],
            [['psm', 'investasi'], 'safe'],
            [['no_induk'], 'string', 'max' => 32],
            [['nama'], 'string', 'max' => 125],
            [['jk'], 'string', 'max' => 1],
            [['kartu'], 'exist', 'skipOnError' => true, 'targetClass' => Kartu::className(), 'targetAttribute' => ['kartu' => 'id_kartu']],
            [['kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['kelas' => 'id_kelas']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_peserta' => 'Id Peserta',
            'no_induk' => 'No Induk',
            'nama' => 'Nama',
            'jk' => 'L/P',
            'psm' => 'PSM',
            'investasi' => 'Investasi',
            'kelas' => 'Kelas',
            'kartu' => 'Kartu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKartuR()
    {
        return $this->hasOne(Kartu::className(), ['id_kartu' => 'kartu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelasR()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSetorans()
    {
        return $this->hasMany(Setoran::className(), ['peserta' => 'id_peserta']);
    }

    public function getInvestasiR()
    {
        return $this->hasMany(Setoran::className(), ['peserta' => 'id_peserta'])
            ->andOnCondition(['tipe' => 1]);
    }

    public function getPsmR()
    {
        return $this->hasMany(Setoran::className(), ['peserta' => 'id_peserta'])
            ->andOnCondition(['tipe' => 2]);
    }

    public static function getArrayMap($field = 'no_induk')
    {
        return ArrayHelper::map(self::find()->all(), 'id_peserta', $field);
    }

    public function beforeSave($insert)
    {
        $this->psm = preg_replace('/\D/', '', $this->psm);
        $this->investasi = preg_replace('/\D/', '', $this->investasi);

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        Setoran::deleteAll(['peserta' => $this->id_peserta]);

        return parent::beforeDelete();
    }
}
