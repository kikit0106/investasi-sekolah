<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_instansi".
 *
 * @property string $id_instansi
 * @property string $nama
 * @property string $alamat
 * @property string $email
 * @property string $telepon
 *
 * @property Kelas[] $kelas
 */
class Instansi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_instansi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'alamat', 'email'], 'required'],
            [['nama', 'email'], 'string', 'max' => 125],
            [['alamat'], 'string', 'max' => 255],
            [['telepon'], 'string', 'max' => 32],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_instansi' => 'Id Instansi',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'email' => 'Email',
            'telepon' => 'Telepon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['instansi' => 'id_instansi']);
    }
}
