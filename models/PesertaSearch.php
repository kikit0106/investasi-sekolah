<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Peserta;

/**
 * PesertaSearch represents the model behind the search form about `app\models\Peserta`.
 */
class PesertaSearch extends Peserta
{
    public $status_bayar, $tanggal, $range1, $range2, $tipe;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['psm', 'investasi', 'kelas', 'kartu'], 'integer'],
            [['no_induk', 'nama', 'jk', 'status_bayar', 'tanggal', 'tipe'], 'safe'],
            [['tanggal'],'required', 'when' => function($model){ return $model->status_bayar != '';}, 'enableClientValidation' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $subQuery = Peserta::find()
            ->select('id_peserta')
            ->joinWith('setorans a');

        if ($this->tanggal) {
            $tgl = explode(' - ', $this->tanggal);
            $this->range1 = isset($tgl[0]) ? date('Y-m-d',strtotime($tgl[0])) : '2017-07-01';
            $this->range2 = isset($tgl[1]) ? date('Y-m-d',strtotime($tgl[1])) : '2018-06-30';

            $subQuery->andFilterWhere(['>=','a.tanggal',$this->range1])
                ->andFilterWhere(['<=','a.tanggal',$this->range2]);
        }

        if ($this->tipe){
            $subQuery->andFilterWhere(['a.tipe' => $this->tipe]);
        }

        if ($this->status_bayar == 2){

            $query = Peserta::find()->orderBy([
                'kelas' => SORT_ASC,
                'nama' => SORT_ASC
            ])->andFilterWhere(['not in','id_peserta',$subQuery]);

            //dump($subQuery->all()); exit();
        } elseif ($this->status_bayar == 1){

            $query = Peserta::find()->orderBy([
                'kelas' => SORT_ASC,
                'nama' => SORT_ASC
            ])->andFilterWhere(['in','id_peserta',$subQuery]);
        } else {

            $query = Peserta::find()->orderBy([
                'kelas' => SORT_ASC,
                'nama' => SORT_ASC
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'psm' => $this->psm,
            'investasi' => $this->investasi,
            'kelas' => $this->kelas,
            'kartu' => $this->kartu,
            'jk' => $this->jk
        ]);

        $query->andFilterWhere(['like', 'no_induk', $this->no_induk])
            ->andFilterWhere(['like', 'nama', $this->nama]);

        return $dataProvider;
    }
}
