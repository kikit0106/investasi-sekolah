<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_profil".
 *
 * @property string $id_profil
 * @property integer $id_user
 * @property string $nama_lengkap
 * @property string $jabatan
 *
 * @property User $user
 */
class Profil extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_profil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user'], 'integer'],
            [['nama_lengkap', 'jabatan'], 'string', 'max' => 64],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_profil' => 'Id Profil',
            'id_user' => 'Id User',
            'nama_lengkap' => 'Nama Lengkap',
            'jabatan' => 'Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
