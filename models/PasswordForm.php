<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 4/7/2017
 * Time: 6:50 AM
 */

namespace app\models;


use yii\base\Model;
use Yii;

class PasswordForm extends Model
{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
        ];
    }

    public function findPasswords($attribute, $params){
        $user = User::findByUsername(Yii::$app->user->identity->username);

        if(!$user->validatePassword($this->oldpass)){
            $this->addError($attribute,'Password lama tidak cocok');
        }
    }

    public function attributeLabels(){
        return [
            'oldpass'=>'Password Lama',
            'newpass'=>'Password Baru',
            'repeatnewpass'=>'Ulangi Password Baru',
        ];
    }
}