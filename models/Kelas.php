<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_kelas".
 *
 * @property string $id_kelas
 * @property string $instansi
 * @property string $nama
 * @property integer $level
 *
 * @property Instansi $instansiR
 * @property Peserta[] $pesertas
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_kelas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instansi', 'level'], 'integer'],
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 32],
            [['instansi'], 'exist', 'skipOnError' => true, 'targetClass' => Instansi::className(), 'targetAttribute' => ['instansi' => 'id_instansi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kelas' => 'Id Kelas',
            'instansi' => 'Instansi',
            'nama' => 'Kelas',
            'level' => 'Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansiR()
    {
        return $this->hasOne(Instansi::className(), ['id_instansi' => 'instansi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblPesertas()
    {
        return $this->hasMany(Peserta::className(), ['kelas' => 'id_kelas']);
    }

    public static function getArrayMap($field = 'nama')
    {
        return ArrayHelper::map(self::find()->all(), 'id_kelas', $field);
    }
}
