<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_kartu".
 *
 * @property string $id_kartu
 * @property string $nama
 *
 * @property Peserta[] $pesertas
 */
class Kartu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_kartu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kartu' => 'Id Kartu',
            'nama' => 'Kartu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['kartu' => 'id_kartu']);
    }

    public static function getArrayMap($field = 'nama')
    {
        return ArrayHelper::map(self::find()->all(), 'id_kartu', $field);
    }
}
