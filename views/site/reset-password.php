<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 4/7/2017
 * Time: 1:40 PM
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
?>
<div class="site-reset-password">

    <div class="box">
        <div class="box-header"></div>
        <div class="box-body with-border">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6" align="center" style="margin-top: 25px;">
                    <p>Masukkan password baru anda</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4">
                    <?php $form = ActiveForm::begin([
                        'id' => 'reset-password-form',
                        'options' => ['class' => 'form-horizontal'],
                    ]); ?>

                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <div class="pull-right">
                            <?= Html::submitButton('Kirim', [
                                'class' => 'btn btn-success'
                            ]) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>