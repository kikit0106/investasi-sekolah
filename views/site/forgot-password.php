<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 4/7/2017
 * Time: 10:12 AM
 */
use yii\bootstrap\Html;
use app\components\Alert;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\User */

$this->title = 'Halaman Lupa Password';
?>
<div class="box">
    <div class="box-header"></div>
    <div class="box-body with-border">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-lg-offset-3 col-lg-6" align="center">
                <?= Alert::widget() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-3 col-lg-6" align="center">
                <p>Masukkan email anda untuk mengirim permintaan reset password anda</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-4 col-lg-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'changepassword-form',
                    'options' => ['class' => 'form-horizontal'],
                ]); ?>

                <?= $form->field($model, 'email', [
                    'inputOptions' => [
                        'placeholder' => 'Masukkan Email Anda.'
                    ]
                ])->textInput() ?>

                <div class="form-group">
                    <div class="pull-right">
                        <?= Html::submitButton('Kirim', [
                            'class' => 'btn btn-success'
                        ]) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>