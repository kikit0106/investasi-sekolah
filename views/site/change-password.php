<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 4/7/2017
 * Time: 7:08 AM
 */

use yii\bootstrap\Html;
use app\components\Alert;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\PasswordForm */

$this->title = 'Ubah Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-password">
    <div class="box">
        <div class="box-header"><?= Alert::widget() ?></div>
        <div class="box-body with-border">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4">
                    <?php $form = ActiveForm::begin([
                        'id' => 'change-password-form',
                        'options' => ['class' => 'form-horizontal'],
                    ]); ?>
                    <?= $form->field($model, 'oldpass', [
                        'inputOptions' => [
                            'placeholder' => 'Password Lama'
                        ]
                    ])->passwordInput() ?>

                    <?= $form->field($model, 'newpass', [
                        'inputOptions' => [
                            'placeholder' => 'Password Baru'
                        ]
                    ])->passwordInput() ?>

                    <?= $form->field($model, 'repeatnewpass', [
                        'inputOptions' => [
                            'placeholder' => 'Ulangi Password Baru'
                        ]
                    ])->passwordInput() ?>

                    <div class="form-group">
                        <div class="pull-right">
                            <?= Html::submitButton('Ubah password', [
                                'class' => 'btn btn-primary'
                            ]) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
