<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */
?>

<style type="text/css">
    .form-group.required .control-label:after {
        content: " *";
        color: #ff6968;
    }

    form {
        padding: 2% 10% 0 10%;
    }
</style>
<div class="peserta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_induk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jk')->dropDownList(['L' => 'L', 'P' => 'P'], ['prompt' => '- Pilih -']) ?>

    <?= $form->field($model, 'psm')->textInput(['class' => 'input-rupiah form-control']) ?>

    <?= $form->field($model, 'investasi')->textInput(['class' => 'input-rupiah form-control']) ?>

    <?= $form->field($model, 'kelas')->dropDownList($kelas, ['prompt' => '- Pilih -']) ?>

    <?= $form->field($model, 'kartu')->dropDownList($kartu, ['prompt' => '- Pilih -']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Back', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
