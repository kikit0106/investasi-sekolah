<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PesertaSearch */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin([
    'id' => 'setoran-form-filter',
    'action' => ['index'],
    'method' => 'get',
]);

$f1 = $form->field($model, 'status_bayar')->dropDownList([2 => 'Belum Bayar', 1 => 'Sudah Bayar'],
    ['prompt' => '- Semua -']);

$f3 = $form->field($model, 'tipe')->dropDownList([1 => 'Investasi', 2 => 'PSM'],
    ['prompt' => '- Semua -']);

$f2 = $form->field($model, 'tanggal')
    ->textInput(['class' => 'form-control range-picker', 'readonly' => true])
    ->label('Rentang Tanggal Setoran');

$btn = Html::submitButton('Search', ['class' => 'btn btn-primary']);

// $btn .= Html::resetButton('Reset', ['class' => 'btn btn-default']);

echo "<div class='row'><div class='col-lg-2'>$f1</div><div class='col-lg-2'>$f3</div><div class='col-lg-4'>$f2</div>"
    . "<div class='col-lg-4' style='margin-top: 2.4%'>$btn</div></div>";

ActiveForm::end();

$js = <<<JS
    var tgl = $('.range-picker');
    var s_bayar = $('#pesertasearch-status_bayar');
    
    tgl.daterangepicker(
        {
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        }
    );
    
    tgl.on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    tgl.on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
    
    if(s_bayar.val() === '') {tgl.attr('disabled','disabled');} //disabled first
    
    s_bayar.on('change',function() {
      if ($(this).val() === '') {tgl.val(''); tgl.attr('disabled','disabled');} else { tgl.removeAttr('disabled'); }
    });
JS;
$this->registerJs($js, \yii\web\View::POS_END);
