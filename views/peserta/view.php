<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */

$this->title = $model->nama;
?>
<div class="peserta-view">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= DetailView::widget([
        'template' => '<tr><th width="25%">{label}</th><td{contentOptions}>{value}</td></tr>',
        'model' => $model,
        'attributes' => [
            'no_induk',
            'nama',
            'kelasR.nama',
            [
                'label' => 'PSM',
                'value' => Helper::rupiah_display($model->psm)
            ],
            [
                'label' => 'Investasi',
                'value' => Helper::rupiah_display($model->investasi)
            ],
        ],
    ]) ?>

    <h5>Investasi</h5>
    <?= $this->render('_tabel', ['relasi' => $model->investasiR]) ?>
    <h5>PSM</h5>
    <?= $this->render('_tabel', ['relasi' => $model->psmR]) ?>

</div>
