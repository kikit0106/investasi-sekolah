<?php

use app\components\Helper;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PesertaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Peserta';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Filter Setoran</h3>
                    <div class="box-tools pull-right">
                        <a type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <?= $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-body">
                    <div class="form-group pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> Create Peserta',
                            ['create'],
                            ['class' => 'btn btn-success']) ?>
                        <?= Html::button('<span class="fa fa-file-excel-o"></span> Download Excel',
                            ['id' => 'excel', 'class' => 'buat btn bg-orange btn-flat']) ?>
                    </div>
                    <?php Pjax::begin([
                        'formSelector' => 'setoran-form-filter'
                    ]); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'header' => 'No',
                                'class' => 'yii\grid\SerialColumn'
                            ],
                            'no_induk',
                            'nama',
                            [
                                'headerOptions' => ['width' => '2%'],
                                'attribute' => 'jk'
                            ],
                            [
                                'attribute' => 'psm',
                                'value' => function ($model) {
                                    return Helper::rupiah_display($model->psm);
                                }
                            ],
                            [
                                'attribute' => 'investasi',
                                'value' => function ($model) {
                                    return Helper::rupiah_display($model->investasi);
                                }
                            ],
                            [
                                'attribute' => 'kelas',
                                'value' => 'kelasR.nama',
                                'filter' => $kelas
                            ],
                            [
                                'attribute' => 'kartu',
                                'value' => 'kartuR.nama',
                                'filter' => $kartu,
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['width' => '6%'],
                                'template' => '{view} {plus} {update} {delete}',
                                'buttons' => [
                                    'update' => function ($url, $model) {
                                        return Helper::tooltipIcon('Edit', 'primary',
                                            ['update', 'id' => $model->id_peserta], []);
                                    },
                                    'delete' => function ($url, $model) {
                                        return Helper::tooltipIcon('Hapus', 'danger',
                                            ['delete', 'id' => $model->id_peserta], ['confirm' => true]);
                                    },
                                    'plus' => function ($url, $model) {
                                        return Helper::tooltipIcon('Setoran', 'success',
                                            ['/setoran/create', 'peserta' => $model->id_peserta], []);
                                    },
                                    'view' => function ($url, $model) {
                                        return Helper::tooltipIcon('Detail', 'primary',
                                            ['view', 'id' => $model->id_peserta], ['modal' => true]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
$('#excel').click(function (e) {
        e.preventDefault();
        var no = $('input[name="PesertaSearch[no_induk]"]').val();
        var nama = $('input[name="PesertaSearch[nama]"]').val();
        var jk = $('input[name="PesertaSearch[jk]"]').val();
        var psm = $('input[name="PesertaSearch[psm]"]').val();
        var inv = $('input[name="PesertaSearch[investasi]"]').val();
        var kls = $('select[name="PesertaSearch[kelas]"]').val();
        var krt = $('select[name="PesertaSearch[kartu]"]').val();
        var tgl = $('input[name="PesertaSearch[tanggal]"]').val();
        var status = $('select[name="PesertaSearch[status_bayar]"]').val();
        var urli = '/peserta/excel?';
        var myObj = {
            PesertaSearch: {
                no_induk: no,
                nama: nama,
                jk: jk,
                psm: psm,
                status_bayar: status,
                investasi: inv,
                kelas: kls,
                kartu: krt,
                tanggal: tgl
            }
        };
        var param = $.param(myObj);
        var newUrl = urli + param;

        window.open(newUrl, '_blank');
        console.log(newUrl);
    });
JS;
$this->registerJs($js, \yii\web\View::POS_END);
