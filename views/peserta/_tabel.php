<?php
/**
 * Created by PhpStorm.
 * User: kikit-pc
 * Date: 10/11/2017
 * Time: 2:57 PM
 *
 * @var $relasi array
 */

use app\components\Helper;

?>
<table class="table table-bordered table-striped" style="margin-bottom: 20px">
    <tbody>
    <?php
    if (sizeof($relasi) == 0){
        echo "<tr><td colspan='4'>Data Setoran Kosong</td></tr>";
    }
    $total = [];
    foreach ($relasi as $key => $data): $total[] = $data->nominal; ?>
        <tr>
            <td><?= $key + 1 ?></td>
            <td><?= Helper::formatDateIndonesia($data->tanggal) ?></td>
            <td><?= Helper::rupiah_display($data->nominal) ?></td>
            <td>
                <?= \yii\helpers\Html::a('Hapus',
                    [ '/setoran/delete','id' => $data->id_setoran ],
                    ['data' =>['confirm' => 'Apakah anda yakin?', 'method' => 'post']]
                    ) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan="2" style="text-align: center">Total</th>
        <th><?= Helper::rupiah_display(array_sum($total)) ?></th>
        <th></th>
    </tr>
    </tfoot>
    <thead>
    <tr>
        <th style="width: 10%">No</th>
        <th style="width: 40%">Tanggal</th>
        <th style="width: 40%">Setoran</th>
        <th style="width: 10%"></th>
    </tr>
    </thead>
</table>
