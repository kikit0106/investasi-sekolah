<?php
/* @var $this \yii\web\View */
/* @var $directoryAsset string */
/* @var $username string */
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/avatar5.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p> <?= ucwords($username) ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Gii',
                        'icon' => 'file-code-o',
                        'url' => ['/gii'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Debug',
                        'icon' => 'dashboard',
                        'url' => ['/debug'],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Pengaturan',
                        'icon' => 'gear',
                        'url' => '#',
                        'items' => [
                            ['label' => 'route', 'icon' => 'gear', 'url' => ['/admin/route'],],
                            ['label' => 'permission', 'icon' => 'gear', 'url' => ['/admin/permission'],],
                            ['label' => 'role', 'icon' => 'gear', 'url' => ['/admin/role'],],
                            ['label' => 'assignment', 'icon' => 'gear', 'url' => ['/admin/assignment'],],
                            ['label' => 'user', 'icon' => 'gear', 'url' => ['/admin/user'],],
                        ],
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => 'Peserta',
                        'icon' => 'flag',
                        'url' => ['/peserta'],
                        'visible' => !Yii::$app->user->isGuest
                    ],

                ],
            ]
        ) ?>

    </section>

</aside>
