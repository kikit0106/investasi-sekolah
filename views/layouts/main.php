<?php
use yii\helpers\Html;
use app\models\User;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    $username = $job = 'Guest';

    if (!Yii::$app->user->isGuest){
        $username = Yii::$app->user->identity->username;
        $job = Html::tag('small',User::findOne(Yii::$app->user->getId())->profil->jabatan);
    }

    app\assets\AppAsset::register($this);
    app\assets\AdminLteAsset::register($this);

    $publishedPath = Yii::$app->assetManager->publish('@vendor/almasaeed2010/adminlte/dist/img/');
    $directoryAsset = $publishedPath[1];
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-green-light sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            compact('directoryAsset','username','job')
        ) ?>

        <?= $this->render(
            'left.php',
            compact('directoryAsset','username')
        ) ?>

        <?=
        $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php

    $this->endBody();

    yii\bootstrap\Modal::begin([
        'headerOptions' => ['id' => 'modalHeader'],
        'closeButton' => ['button', 'x'],
        'id' => 'detailModal',
        'size' => 'modal-md',
        'class' => 'modal',
        'clientOptions' => ['backdrop' => 'static']
    ]);
    echo "<div id='modalContent'><div class='overlay' align='center'><h2><i class='fa fa-spinner fa-spin'></i></h2></div></div>";
    yii\bootstrap\Modal::end(); ?>

    </body>
    <script type="application/javascript">
        window.setTimeout(function() { $(".alert-success").alert('close'); }, 5000);
        $(".modalButton").click(function (e) {
            e.preventDefault();
            $("#detailModal").modal('show').find("#modalContent").load($(this).attr('href'));
        });
        $('#detailModal').on('hide.bs.modal', function (e) {
            $('#detailModal').find("#modalContent").html("<div class='overlay' align='center'><h2><i " +
                "class='fa fa-spinner fa-spin'></i></h2></div>");
        });
    </script>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
