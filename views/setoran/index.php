<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Setoran;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SetoranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Setorans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setoran-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Setoran', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_setoran',
            'peserta',
            'nominal',
            'keterangan',
            [
                'attribute' => 'tipe',
                'filter' => Setoran::TIPE,
                'value' => function($model){ return Setoran::TIPE[$model->tipe]; }
            ],
            'tanggal',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
