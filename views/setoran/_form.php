<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Setoran;

/* @var $this yii\web\View */
/* @var $model app\models\Setoran */
/* @var $form yii\widgets\ActiveForm */
?>
    <style type="text/css">
        .form-group.required .control-label:after {
            content: " *";
            color: #ff6968;
        }

        form {
            padding: 2% 10% 0 10%;
        }
    </style>
    <div class="setoran-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'tanggal', [
            'template' => '{label}
                      <div class="input-group"><div class="input-group-addon">
                      <i class="fa fa-calendar"></i></div>{input}</div>
                      {hint}{error}',
            'labelOptions' => ['class' => 'control-label']
        ])->textInput(['class' => 'form-control tgl-input', 'readonly' => true]) ?>

        <?= $form->field($model, 'tipe')->dropDownList(Setoran::TIPE, ['prompt' => '- Pilih -']) ?>

        <?= $form->field($model, 'nominal')->textInput(['maxlength' => true, 'class' => 'form-control input-rupiah']) ?>

        <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'peserta')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Back', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$js = <<<JS
$('.tgl-input').datepicker({
    format: 'dd-mm-yyyy',
    changeMonth: true,
    changeYear: true,
    autoclose: true,
    todayBtn: true,
    language: 'id'
});
JS;
$this->registerJs($js, \yii\web\View::POS_END);
